const express = require('express')
const path = require('path')
const fs = require('fs')
const app = express()
const port = 3000
var morgan = require('morgan')
app.use(express.json())

app.set('view engine', 'ejs');

const createPath = (page) => { return path.resolve(__dirname, 'views', `${page}.ejs`) }



let users = [{ name: "user1", color: "red" }, { name: "user2", color: "blue" }, { name: "user3", color: "green" }];

//app.use(express.static('public')) //Добавление папки из которой можно передавать данные

//Логгирование запросов 
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))


app.get('/:page', (req, res, next) => {
    let path = createPath(req.params.page);
    if (fs.existsSync(path)) res.render(path, {params: req.body})
    else next()
})




//Проверка ошибок
app.use("/users/change/*", (req, res, next) => {
    const { name, color } = req.body;
    if (name == '' || color == '')
        res.status(400).send("Неправильные данные");
    next();
})

//Проверка авторизации
app.use((req, res, next) => {
    let user = req.body.user;
    if (user == null || undefined) {
        res.status(401).send("Ошибка авторизации");
    } else {
        return next();
    }
})

app.get('/users/all/get', (req, res, err) => {
    res.send(users);
})
app.put('/users/change/add', (req, res) => {
    if (users.find(((obj) => { return req.body.name == obj.name })) != undefined)
        res.status(400).send("Пользователь существует");
    else {
        users.push({ name: req.body.name, color: color });
        res.status(200).send(users);
    }
})
app.delete('/users/change/delete', (req, res) => {
    const index = users.findIndex(((obj) => { return req.body.name == obj.name }));
    if (index != -1) {
        const f1 = index == 0 ? [] : users.slice(0, index);
        const f2 = index == users.length - 1 ? [] : users.slice(index + 1, users.length);
        users = f1.concat(f2);
        res.status(200).send(users);
    }
    else {
        res.status(400).send("Пользователь не существует");
    }
})
app.listen(port, () => {
    console.log(`Example app listening on port http://localhost:${port}`)
})

